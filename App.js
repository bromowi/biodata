import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./homepage/components/home";
import About from "./homepage/components/About";
import Contact from "./homepage/components/Contact";
import Article from "./homepage/components/article";
function App() {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
           <Home />
        </Route>
        <Route path="/article" exact>
          <Article/>
        </Route>
        <Route path="/about" exact>
          <About />
        </Route>
       <Route path="/contact" exact>
        <Contact />
        </Route>
      </Switch>
    </Router>
  
  
  );
}

export default App;