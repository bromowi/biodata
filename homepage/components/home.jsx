import React, {useState} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Routes from "../Routes/router"
import { 
  Button,
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup, InputGroupAddon, InputGroupText, Input
} from "reactstrap";
import { Link } from "react-router-dom";



const Bromo = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const navbar = {
    height: '100px',
    position: 'relative',
    top: '0',
    width: 'relative',
    zIndex: '999'
  }
  const inputGroup = {
    width: '300px',
    marginRight: '10px'
  };
  const webTitle = {
    fontSize: '35px',
    fontWeight: 'bold',
    marginLeft: '20px',
    fontFamily: 'monospace'
  }


  const fontSmall = {
    fontFamily:'monospace', 
    fontWeight:'bold', 
    fontSize:'16px'
  };
  const fontBig = {
    fontFamily:'monospace', 
    fontWeight:'bold', 
    fontSize:'50px'
  };
  const wrapper = {
    display: 'flex',
    marginTop: '100px',
    fontFamily: 'monospace'
  }


  return (
    <div>
    <Container>
       <Navbar color="light" light expand="md" style={navbar}>
        <NavbarBrand style={webTitle}>Bromo</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
          <NavItem>
              <NavLink href="/article">Article</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/about">About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/Contact">Contact</NavLink>
            </NavItem>
          </Nav>
          <InputGroup style={inputGroup}>
            <Input />
            <InputGroupAddon addonType="append">
              <Button color="warning"><i class="fas fa-search"></i></Button>
            </InputGroupAddon>
          </InputGroup>
          <Button href='/' color="warning">BALIK !</Button>
        </Collapse>
      </Navbar>
    </Container>
    </div>
    
    
  )
};

export default Bromo;
