import React, {useEffect, useState} from 'react';
import { Button,
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  InputButton,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from 'reactstrap';
import "bootstrap/dist/css/bootstrap.min.css";
import Routes from "../Routes/router";
import { Link } from "react-router-dom";

const Example = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const navbar = {
    height: '100px',
    position: 'relative',
    top: '0',
    width: 'relative',
    zIndex: '999'
  }
  const inputGroup = {
    width: '300px',
    marginRight: '10px'
  };
  const webTitle = {
    fontSize: '35px',
    fontWeight: 'bold',
    marginLeft: '20px',
    fontFamily: 'monospace'
  }


  const fontSmall = {
    fontFamily:'monospace', 
    fontWeight:'bold', 
    fontSize:'16px'
  };
  const fontBig = {
    fontFamily:'monospace', 
    fontWeight:'bold', 
    fontSize:'50px'
  };
  const wrapper = {
    display: 'flex',
    marginTop: '100px',
    fontFamily: 'monospace'
  }

  return (
    <>
    <div>
    <Container>
       <Navbar color="light" light expand="md" style={navbar}>
        <NavbarBrand href="/home" style={webTitle}>Bromo</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
          <NavItem>
              <NavLink href="/home">Article</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/about">About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/Contact">Contact</NavLink>
            </NavItem>
          </Nav>
          <InputGroup style={inputGroup}>
            <Input />
            <InputGroupAddon addonType="append">
              <Button color="warning"><i class="fas fa-search"></i></Button>
            </InputGroupAddon>
          </InputGroup>
          <Button href='/' color="warning">BALIK !</Button>
        </Collapse>
      </Navbar>
    </Container>
    </div>

    <div>
    <Form>
      <FormGroup>
        <Label for="exampleEmail">Email</Label>
        <Input type="email" name="email" id="exampleEmail" placeholder="with a placeholder" />
      </FormGroup>
      <FormGroup>
        <Label for="exampleText">Text Area</Label>
        <Input type="textarea" name="text" id="exampleText" />
      </FormGroup>
      <FormGroup check>
        <Label check>
          <Input type="checkbox" />{' '}
          Check me out
        </Label>
      </FormGroup>
      <Button>Submit</Button>
    </Form>
    </div>
    </>
  );
}

export default Example;